%include "lib.inc"
%include "dict.inc"
%include "words.inc"

%define SIZE 256

section .bss
buffer: resb 256

section .rodata
read_err_msg: db "An error occurred while reading the string", 0
find_err_msg: db "No such word in the dict", 0

section .text
global _start

_start:
    mov rdi, buffer
    mov rsi, SIZE
    call read_word
    test rax, rax
    jz .read_fail

    mov rdi, buffer
    mov rsi, first_word
    call find_word
    test rax, rax
    jz .find_fail

    mov rdi, rax
    add rdi, 8
    push rdi
    call string_length
    pop rdi
    add rdi, rax
    inc rdi
    call print_string
    call print_newline
    xor rdi, rdi
    call exit
    
   .read_fail:
        mov rdi, read_err_msg
        jmp .common_err

   .find_fail:
        mov rdi, find_err_msg

   .common_err:
        call print_err
        call print_newline
        call exit
