EXECUTABLE = my_program

ASM_FILES = dict.asm lib.asm main.asm

OBJ_FILES = $(ASM_FILES:.asm=.o)

NASM = nasm
NASMFLAGS = -f elf64

LD = ld
LDFLAGS =

PYTHON = python3
TEST_SCRIPT = main.py

all: $(EXECUTABLE)

%.o: %.asm
	$(NASM) $(NASMFLAGS) $< -o $@

$(EXECUTABLE): $(OBJ_FILES)
	$(LD) $(LDFLAGS) $^ -o $@

test: $(EXECUTABLE)
	$(PYTHON) $(TEST_SCRIPT)

clean:
	$(RM) $(OBJ_FILES) $(EXECUTABLE)

rebuild: clean all
.PHONY: all test clean rebuild
