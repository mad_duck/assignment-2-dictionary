import subprocess

const COUNT_STRING = 100

input_data = ["third_word", "second_word", "first_word", "seitmo",
              "pum" * COUNT_STRING
              "ump" * COUNT_STRING
              "itmo" * COUNT_STRING]

success_codes = ["Wow what a wonderful sentence", "PiuPiuPiu! oh no i am dead", "How are you? Brilliant", "", ""]

error_codes = ["", "", "", "No such word in the dict", "An error occurred while reading the string"]

path = "./my_program"

for i, test_input in enumerate(input_data):
    process = subprocess.run(path, input=test_input, text=True, capture_output=True)

    stderr = process.stderr.strip()
    stdout = process.stdout.strip()

    if stderr != error_codes[i]:
        print(f"Test{i + 1} failed: got {stderr}, expected {error_codes[i]}")
    elif stdout != success_codes[i]:
        print(f"Test{i + 1} failed: got {stdout}, expected {success_codes[i]}")
    else:
        print(f"Test{i + 1} passed!")

    print("--------------------------")
